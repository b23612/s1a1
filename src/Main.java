// Main class
    //entry point for our java program
    // main class has 1 method inside, tha "main method" => run code


public class Main {
    public static void main(String[] args) {
        // a statement that allows us to print the value of arguments passed into its terminal
        System.out.println("Hello world!");
    }
}